package parallelStream;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MonteCarlo {

    public static Map<Integer, Double> parallelDiceRolls(int iterations) {
        double fraction = 1.0 / iterations;
        return IntStream.range(0, iterations)
                .parallel()
                .mapToObj(twoDiceThrows())
                .collect(Collectors.groupingBy(v -> v, Collectors.summingDouble(n -> fraction)));
    }

    private static IntFunction<Integer> twoDiceThrows() {
        return i -> {
            ThreadLocalRandom random = ThreadLocalRandom.current();
            int firstThrow = random.nextInt(1, 7);
            int secondThrow = random.nextInt(1, 7);
            return firstThrow + secondThrow;
        };
    }

    public static double parallelPi(int iterations) {
        long count = IntStream.range(0, iterations)
                .parallel()
                .mapToObj(piCount())
                .filter(v -> v == 1).count();
        return count * 4.0 / iterations;
    }
    private static IntFunction<Integer> piCount() {
        return i -> {
            double x = Math.random();
            double y = Math.random();
            return (x * x + y * y <= 1) ? 1 : 0;
        };
    }
}