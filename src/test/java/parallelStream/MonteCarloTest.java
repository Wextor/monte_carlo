package parallelStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

public class MonteCarloTest {

    @Test
    public void parallelDiceRolls() {
        Map<Integer, Double> integerDoubleMap = MonteCarlo.parallelDiceRolls(10000000);
        double sum = integerDoubleMap.values().stream().reduce(0d, (a, b) -> a + b);
        long intSum = Math.round(sum);
        Assertions.assertEquals(intSum, 1);
    }

    @Test
    public void parallelPi_1M() {
        double pi = MonteCarlo.parallelPi(1000000);
        Assertions.assertTrue(pi > 3.1 && pi < 3.2);
    }

    @Test
    public void parallelPi_1K() {
        double pi = MonteCarlo.parallelPi(1000);
        Assertions.assertTrue(pi > 2.9 && pi < 3.4);
    }
}